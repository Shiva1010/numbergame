<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NG 遊戲房間</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&family=Gloria+Hallelujah&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mystery+Quest&display=swap" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{URL::asset('js/gametime.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/user.show.js')}}"></script>
</head>

<body>

<style>
    hr.style-five {
        border: 0;
        height: 0;
        box-shadow: 0 0 150px 30px  #750000;
    }
    hr.style-five:after {  /* Not really supposed to work, but does */
        content: "\00a0";  /* Prevent margin collapse */
    }
    hr.style-01 {
        border: 0;
        height: 0;
        box-shadow: 0 0 15px 1px #750000;
    }
    hr.style-02 {
        border: 0;
        height: 0;
        box-shadow: 0 0 15px 1px #272727;
    }
    gfont01{
        font-family:'Cabin Sketch', cursive;
        font-size: 100px;
    }
    gfont02{
        font-family: 'Cabin Sketch', cursive;
        font-family: 'Gloria Hallelujah', cursive;
        font-size: 30px
    }
    gfont03{
        font-family: 'Mystery Quest', cursive;
        font-size: 35px
    }
    gfont04{
        font-family: 'Cabin Sketch', cursive;
        font-family: 'Gloria Hallelujah', cursive;
        font-size: 50px
    }

</style>

<hr class="style-five"/>

<div class="col-sm-12" style="text-align: center">
    <gfont01>I Want To Play A Game</gfont01>
    <br><br>
    <gfont04 id='user_date'></gfont04>
    <gfont02 id='user_score'></gfont02>
    <br>
    <button id="gameStart" type="button" class="btn btn-dark" style="width:250px;height:70px" >
        <gfont02>Game Start</gfont02>
    </button>
    <br><br><br>
<div class="col-12" style="text-align: center">
    <hr class="style-02"/>
    <br>
    <gfont03>
        <p id="roomId"></p>
        <hr class="style-02"/>
        <br>
        <p id="nowPlayer1"></p>
        <p id="number1"></p>
        <br>
        <hr class="style-01"/>
        <br>
        <p id="nowPlayer2"></p>
        <p id="number2"></p>
    </gfont03>
</div>
    <br>
    <hr class="style-02"/>
    <br><br><br>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">

    //  持續請求，瞭解房間狀態
    // onload = 要觸發的 javascript function
    onload = function () {
        getRoom();
    }

    function getRoom() {
        // 每 10 秒更新一次
        setTimeout(getRoom, 10 * 1000);
        var key = JSON.parse(localStorage.getItem('token'));
        var roomId = JSON.parse(localStorage.getItem('roomId'));
        $.ajax({
            url: '/api/game/roomdate/' + roomId,  // roomId 房號索引用
            method: 'GET',
            dataType: 'json',
            beforeSend: function (request) {
                console.log(request);
                request.setRequestHeader('Authorization', `Bearer ${key}`);
            },
            success: function (nowRoom) {

                console.log(nowRoom);
                $('#roomId').html("RoomNo：" + nowRoom.date[0]['id']);
                $('#nowPlayer1').html("Player 1：" + nowRoom.date[0]['player1']);
                $('#number1').html("P1 Number：" + nowRoom.date[0]['number1']);
                $('#nowPlayer2').html("Player 2：" + nowRoom.date[0]['player2']);
                $('#number2').html("P2 Number：" + nowRoom.date[0]['number2']);


            }, error: function (err) {
                console.log(err);
            }
        })

        $.ajax({
            url: '/api/game/over/' + roomId,  // roomId 房號索引用
            method: 'GET',
            dataType: 'json',
            beforeSend: function (request) {
                console.log(request);
                request.setRequestHeader('Authorization', `Bearer ${key}`);
            },
            success: function (end) {
                if(end.wait == 'end'){
                    alert(end.message);
                    location.href = '/gamelobby';
                }
                console.log(end);

            }, error: function (err) {
                console.log(err);
            }
        })
    }

</script>


</div>

<br><br><br><br><br>
<hr class="style-five"/>

</body>
</html>



