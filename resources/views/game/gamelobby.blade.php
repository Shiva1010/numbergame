<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>等待室</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&family=Gloria+Hallelujah&display=swap" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{URL::asset('js/game.start.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/user.show.js')}}"></script>

</head>
<body>

<style>
    hr.style-five {
        border: 0;
        height: 0;
        box-shadow: 0 0 80px 20px  #5B5B5B;
    }
    hr.style-five:after {  /* Not really supposed to work, but does */
        content: "\00a0";  /* Prevent margin collapse */
    }
    gfont01{
        font-family:'Cabin Sketch', cursive;
        font-size: 100px;
    }
    gfont02{
        font-family: 'Cabin Sketch', cursive;
        font-family: 'Gloria Hallelujah', cursive;
        font-size: 25px
    }
</style>

<hr class="style-five"/>

<div class="container">
    <div class="row align-items" style="text-align: center">
        <div class="col-sm-8" style="text-align: left">
            <span>
                <gfont01>Game Ranking</gfont01>
                <br><br>
            </span>
        </div>
        <div class="col">
            <span>
            </span>
        </div>
    </div>
</div>

<div class="container">
    <div class="row align-items" style="text-align: left">
        <div class="col-sm-7" style="text-align: left">
            <span>
                <gfont02 id='leaderboard'></gfont02>
            </span>
        </div>
        <div class="col">
            <span>
                <gfont02 id='user_date'></gfont02>
                    <input type="button" class="btn btn-secondary btn" id ="joinRoom" value="Join Room" >
                    <input type="button" class="btn btn-secondary btn" id ="logout" value="Logout" >
            </span>
        </div>
    </div>
</div>


</body>
</html>






