<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NG最高後台管理</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&family=Gloria+Hallelujah&display=swap" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{URL::asset('js/admin.alluser.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/admin.logout.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/admin.rank.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/admin.show.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/admin.high.js')}}"></script>

</head>
<body  style="background-color:	black;">  // 使用黑背景

<style>
    hr.style-five {
        border: 0;
        height: 0; /* Firefox... */
        box-shadow: 0 0 10px 2px 		#AE8F00;
    }
    hr.style-five:after {  /* Not really supposed to work, but does */
        content: "\00a0";  /* Prevent margin collapse */
    }
    gfont01{
        font-family:'Cabin Sketch', cursive;
        font-size: 130px ;
        color: #930000;
    }
    gfont02{
        font-family: 'Cabin Sketch', cursive;
        font-family: 'Gloria Hallelujah', cursive;
        font-size: 50px;
    }
    gfont03{
        font-family: 'Cabin Sketch', cursive;
        font-family: 'Gloria Hallelujah', cursive;
        font-size: 30px;
    }
    gfont04{
        font-family: 'Cabin Sketch', cursive;
        font-family: 'Gloria Hallelujah', cursive;
        font-size: 50px;
        color: #666699;
    }
</style>


<hr class="style-five"/>

<div style="text-align: center">
    <gfont01>NG High Backstage</gfont01><br><br>

    <gfont04  id='admin_date'></gfont04>


    <button type="button" class="btn btn-outline;"  style="border:0; width:300px;height:90px;color:#999900;" id="lookUser" >
        <gfont02>All Users</gfont02>
    </button>

    <button type="button" class="btn btn-outline;"  style="border:0; width:300px;height:90px;color:#999900;" id="adminRank" >
        <gfont02>Rank</gfont02>
    </button>

    <button type="button" class="btn btn-outline;" style="border:0; width:300px;height:90px;color:#999900;" id="adminLogout" >
        <gfont02>Logout</gfont02>
    </button>

</div>

<br>
<hr class="style-five"/>
<br><br>


<gfont03 id='backstage'></gfont03>


</body>
</html>

