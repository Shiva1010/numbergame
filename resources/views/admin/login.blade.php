<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NG管理員註冊頁</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&family=Gloria+Hallelujah&display=swap" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{URL::asset('js/admin.login.js')}}"></script>

</head>
<body  style="background-color:	black;">

<input id='url' type='hidden' value='http://82e096c3e7aa.ngrok.io' >

<style>
    hr.style-five {
        border: 0;
        height: 0; /* Firefox... */
        box-shadow: 0 0 10px 2px 		#AE8F00;
    }
    hr.style-five:after {  /* Not really supposed to work, but does */
        content: "\00a0";  /* Prevent margin collapse */
    }
    gfont01{
        font-family:'Cabin Sketch', cursive;
        font-size: 130px ;
        color: cadetblue;
    }
    gfont02{
        font-family: 'Cabin Sketch', cursive;
        font-family: 'Gloria Hallelujah', cursive;
        font-size: 35px;
        color: #FFFF6F;
    }
</style>

<hr class="style-five"/>

<div style="text-align: center">
    <gfont01>NG Admin Login</gfont01><br><br>

        <div class="form-group row">
            <label for="colFormLabel" class="col-sm-4 col-form-label"></label>
            <div class="col-sm-3">
                <gfont02>ACCOUNT</gfont02>
                <input type="text" class="form-control" name='account' id='loginAccount'/>
                <br><br>
            </div>
        </div>
        <div class="form-group row">
            <label for="colFormLabel" class="col-sm-6 col-form-label"></label>
            <div class="col-sm-3">
                <gfont02>PASSWORD</gfont02>
                <input type="password" class="form-control" name='password' id='loginPassword'/>
                <br><br>
            </div>
        </div>
        <div class="form-group row">
            <label for="colFormLabel" class="col-sm-8 col-form-label"></label>
            <div class="col-sm-3">
                <input type="button" class="btn btn-secondary btn" id="adminLogin"   value="Login">
                <input type="button" class="btn btn-secondary btn"   value="Home" onclick="javascript:location.href='/admin/index'" >
            </div>
        </div>


    <br><br>



</div>

<br>

<hr class="style-five"/>

<br><br>


</body>
</html>

