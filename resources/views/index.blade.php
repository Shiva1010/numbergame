<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NG首頁</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&family=Gloria+Hallelujah&display=swap" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{URL::asset('js/user.login.js')}}"></script>


</head>
<body>

<input id='url' type='hidden' value='http://82e096c3e7aa.ngrok.io' >

<style>
    hr.style-five {
        border: 0;
        height: 0; /* Firefox... */
        box-shadow: 0 0 10px 2px 		#AE8F00;
    }
    hr.style-five:after {  /* Not really supposed to work, but does */
        content: "\00a0";  /* Prevent margin collapse */
    }
    gfont01{
        font-family:'Cabin Sketch', cursive;
        font-size: 130px;
    }
    gfont02{
        font-family: 'Cabin Sketch', cursive;
        font-family: 'Gloria Hallelujah', cursive;
        font-size: 35px;
    }
</style>

<hr class="style-five"/>

<div style="text-align: center">
    <gfont01>NG Club</gfont01><br><br>


    <button type="button" class="btn btn-outline-info" style="width:170px;height:90px" onclick="javascript:location.href='/register'" >
        <gfont02>register</gfont02>
    </button>


    <button type="button" class="btn btn-outline-success" style="width:170px;height:90px;" onclick="javascript:location.href='/login'">
        <gfont02>login</gfont02>
    </button>

</div>

<br>

<hr class="style-five"/>


</body>
</html>
