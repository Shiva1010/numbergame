<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use App\User;
use App\Room;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
    public function index()
    {
        $showAll = User::orderBy('score','desc')
            ->get(['id','account','score','competition','win']);

        return response()->json(['message' => '遊戲排名', 'date' => $showAll]);
    }

    public function join()
    {
        $user = Auth::user();
        $account = $user -> account;
        $status = $user -> status;

        if($status == 'down'){
            return response()->json(['message' => '停權中']);
        }

//        $now = Carbon::now('Asia/Taipei');

//       時間戳轉字串
//        $now = Carbon::now(+8)->toDateTimeString();
//        $now = Carbon::now(+8);

        $checkPlayer = Room::where('player2', NULL)->first();


        if($checkPlayer)
        {
            $nowPlayer01 = $checkPlayer->player1;

            if($nowPlayer01 == $account){
                return response()->json(['message'=>'等待其他玩家','date'=>$nowPlayer01]);
            }

                $checkPlayer->update
                ([
                    'player2' => $account,
                ]);

                return response()->json(['message' => '加入遊戲成功', 'date' => $checkPlayer]);

        }

            $create = Room::create
            ([
                'player1' => $account,
            ]);

            return response()->json(['message' => '新遊戲房建立', 'date' => $create]);
    }

    public function roomDate($id)
    {
        $room = Room::where('id',$id)->get();

        return response()->json(['message' => '遊戲房間狀況', 'date' => $room]);
    }

    public function havenumber($id)
    {
        $user = Auth::user();
        $nowRoom = Room::where('id',$id)->first();

        $account = $user -> account;
        $player1 = $nowRoom -> player1;
        $player2 = $nowRoom -> player2;
        $number1 = $nowRoom -> number1;
        $number2 = $nowRoom -> number2;

        $number = rand(1,9);

        if($account == $player1 & $number1 == NULL )
        {
            $nowRoom->update
            ([
                'number1' => $number,
            ]);
            return response() -> json(['message' => 'P1玩家取得數字', 'date' => [$number1,$number2]]);
        }

        if($account == $player2 && $number2 == NULL )
        {
            $nowRoom->update
            ([
                'number2' => $number,
            ]);
            return response() -> json(['message' => 'P2玩家取得數字', 'date' => [$number1, $number2]]);
        }

        return response() -> json(['message' => '已經取得數字了','date' => [$number1, $number2]]);


    }


    public function over($id){
        $user = Auth::user();
        $nowRoom = Room::where('id',$id)->first();

        $account = $user -> account;
        $player1 = $nowRoom -> player1;
        $player2 = $nowRoom -> player2;
        $number1 = $nowRoom -> number1;
        $number2 = $nowRoom -> number2;

        $end1 = User::where('account',$player1)->first();
        $end2 = User::where('account',$player2)->first();

        $competition01 = $end1 -> competition + 1;
        $competition02 = $end2 -> competition + 1;

        if($number1 && $number2){
            if ($number1 > $number2){

               if($nowRoom->winner == NULL) {
                   $end1->update([
                       'score' => $end1->score + 10,
                       'competition' => $competition01,
                       'win' => $end1->win + 1,
                   ]);

                   $end2->update([
                       'score' => $end2->score - 5,
                       'competition' => $competition02,
                   ]);

                   $nowRoom->update([
                       'winner' => $player1,
                   ]);
                   if ($end2->score < 0){
                       $end2->update(['score' => 0]);
                       return response()->json(['message' => 'P1 win，P2 lose','wait'=>'end']);
                   }
                   return response()->json(['message' => 'P1 win，P2 lose','wait'=>'end']);
               }
                return response()->json(['message' => 'P1 win，P2 lose','wait'=>'end']);
            }else if ($number1 < $number2){

                if($nowRoom->winner == NULL) {
                    $end2->update([
                        'score' => $end2 -> score + 10,
                        'competition' => $competition02,
                        'win' => $end2 -> win + 1,
                    ]);
                    $end1->update([
                        'score' => $end1 -> score - 5,
                        'competition' => $competition01,
                    ]);
                    $nowRoom->update([
                        'winner' => $player2,
                    ]);

                    if ($end1->score < 0){
                        $end1->update(['score' => 0]);
                        return response()->json(['message' => 'P2 win，P1 lose','wait'=>'end']);
                    }
                    return response()->json(['message' => 'P2 win，P1 lose','wait'=>'end']);
                }
                return response()->json(['message' => 'P2 win，P1 lose','wait'=>'end']);
            }else{
                if($nowRoom->winner == NULL) {
                    $end1->update([
                        'competition' => $competition01,
                    ]);

                    $end2->update([
                        'competition' => $competition02,
                    ]);

                    $nowRoom->update([
                        'winner' => "Draw",
                    ]);
                    return response()->json(['message' => 'Draw','wait'=>'end']);
                }
                return response()->json(['message' => 'Draw','wait'=>'end']);
            }
        }
        return response()->json(['message' => 'waiting','wait'=>'running']);
    }

    public function result(Request $request)
    {
        $user = Auth::user();
        $room_id = $request['id'];
        $gameEnd = $request['gameEnd'];

        $nowRoom = Room::where('id',$room_id)->first();

        $score = $user -> score;
        $competition = $user -> ccompetition;
        $win = $user -> win;

        $addScor = $score + 10;
        $addCompetition = $competition + 1;
        $addWin = $win + 1;
        $minusScore = $score - 5;

        if($gameEnd == 'win' )
        {
            $user -> update
           ([
               'score' => $addScor,
               'competition' => $addCompetition,
               'win' => $addWin,
           ]);

           $nowRoom -> update
           ([
               'winner' => $user -> account,
           ]);

            return response()->json(['message' => '遊戲結果 You Win', 'date' => $user]);
        }

        if($gameEnd == 'lose')
        {
            $user -> update
            ([
                'score' => $minusScore,
                'competition' => $addWin,
            ]);

            return response()->json(['message' => '遊戲結果 You lose', 'date' => $user]);
        }
    }

    public function play(Request $request)
    {
        $user = Auth::user();
        $room_id = $request['id'];
        $nowRoom = Room::where('id',$room_id)->first();

        $account = $user->account;
        $nowRoomPlayer1 = $nowRoom -> player1;

        $number1 = rand(1,9);
        $number2 = rand(1,9);

        $score = $user -> score;
        $competition = $user -> ccompetition;
        $win = $user -> win;

        $addScore = $score + 10;
        $addCompetition = $competition + 1;
        $addWin = $win + 1;
        $minusScore = $score - 5;



        if($number1 > $number2)
        {
            if($account == $nowRoomPlayer1)
            {
                $user -> update
                ([
                    'score' => $addScore,
                    'competition' => $addCompetition,
                    'win' => $addWin,
                ]);

                $nowRoom -> update
                ([
                    'winner' => $user -> account,
                ]);

                return response()->json
                ([
                    'message' => 'you win',
                    'number1' => $number1,
                    'number2' => $number2
                ]);
            }else{
                $user -> update
                ([
                    'score' => $minusScore,
                    'competition' => $addWin,
                ]);

                return response()->json
                ([
                    'message' => 'you lose',
                    'number1' => $number1,
                    'number2' => $number2
                ]);
            }
        }elseif($number2 > $number1) {
            if(!$account == $nowRoomPlayer1)
            {
                $user->update
                ([
                    'score' => $addScore,
                    'competition' => $addCompetition,
                    'win' => $addWin,
                ]);

                $nowRoom->update
                ([
                    'winner' => $user->account,
                ]);

                return response()->json
                ([
                    'message' => 'you win',
                    'number1' => $number1,
                    'number2' => $number2
                ]);
            } else {
                $user->update
                ([
                    'score' => $minusScore,
                    'competition' => $addWin,
                ]);

                return response()->json
                ([
                    'message' => 'you lose',
                    'number1' => $number1,
                    'number2' => $number2
                ]);
            }


        }



        return response()->json
        ([
            'message' => '平手囉',
            'number1' => $number1,
            'number2' => $number2
        ]);

    }
}
