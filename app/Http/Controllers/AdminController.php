<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Admin;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{

    public function login(Request $request)
    {
        $rules = [
            'account' => 'required|min:2',
            'password' => 'required|min:6|max:12',
        ];

        $input = $request->all();
        $valitaor=Validator::make($input, $rules);

        if($valitaor->fails())
        {
            return response()->json(['message' => $valitaor->errors()], 403);
        }

        $check_account = Admin::where('account', $request->account)->first();

        if($check_account == null)
        {
            return response()->json(['message' => '此管理帳戶尚未註冊'], 403);
        }

        $getHashPwd = $check_account -> password;

        $pwd = $request->password;

        if(!Hash::check($pwd, $getHashPwd))
        {
            return response()->json(['message' => '輸入的帳號或密碼錯誤']);
        }

        $check_account -> update(['api_token' => str::random(10)]);
        $now_account = Admin::where('account', $request->account)->first();

        return response()->json
        ([
            'message' => '登入成功',
            'date' => $now_account,
        ]);

    }

    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $check_account = Admin::where('account',$request->account)->first();

        if(!$check_account == null)
        {
            return response()->json(['message' => '此管理帳戶已經被註冊了'],403);
        }

        $rules = [
            'name' => 'required|min:2|max:20',
            'account' => 'required|min:2',
            'password' => 'required|min:6|max:12',
        ];

        $input = $request->all();

        $validator = Validator::make($input, $rules);

        if ($validator -> fails())
        {
            return response()->json(['message' => $validator->errors()],403);
        }

        $HashPwd = Hash::make($request['password']);

        $create=Admin::create([
            'name' => $request['name'],
            'account' => $request['account'],
            'password' => $HashPwd,
        ]);
        return response()->json
        ([
            'message'=>'管理員帳戶註冊成功',
            'date'=>$create,
        ]);
    }


    public function logout()
    {
        $admin = Auth::user();
        $removeToken = NULL;

        $admin->update(['api_token' => $removeToken]);

        return response()->json(['message' => $admin]);
    }
    public function show()
    {
        $admin = Auth::user();

        return response()->json(["message" => "當前管理者", "date" =>$admin]);

    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
