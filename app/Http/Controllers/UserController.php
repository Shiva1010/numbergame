<?php

namespace App\Http\Controllers;


use http\Env\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $check_account = User::where('account',$request['account'])->first();

        if($check_account == null)
        {
            $rules = [
                'name' => 'required|regex:/(^(?!(?i)admin)(?!(?i)draw)(?!(?i)close)[a-zA-Z]\w*$)/|min:2|max:20',
                'account' => 'required|regex:/(^(?!(?i)admin)(?!(?i)draw)(?!(?i)close)[a-zA-Z]\w*$)/|min:4',
                'password' => 'required|regex:(^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,20}$)',
            ];

            $input = $request->all();

            $validator = Validator::make($input, $rules);

            if ($validator -> fails())
            {
                return response()->json(['message' => $validator->errors()],403);
            }

            $HashPwd = Hash::make($request['password']);

            $create=User::create([
                'name' => $request['name'],
                'account' => $request['account'],
                'password' => $HashPwd,
                'score' => 0,
                'competition' => 0,
                'win' => 0,
            ]);


            return response()->json
            ([
                'message'=>'管理員帳戶註冊成功',
                'date'=>$create,
            ]);
        }

        return response()->json(['message' => '此帳戶已經被註冊了'],403);
    }


    public function login(Request $request)
    {
        $rules = [
            'account' => 'required|min:2',
            'password' => 'required|min:6|max:12',
        ];

        $input = $request->all();
        $valitaor=Validator::make($input, $rules);

        if($valitaor->fails())
        {
            return response()->json(['message' => $valitaor->errors()], 403);
        }

        $check_account = User::where('account', $request->account)->first();

        if($check_account == null)
        {
            return response()->json(['message' => '此帳戶尚未註冊'], 403);
        }

        $getHashPwd = $check_account -> password;

        $pwd = $request->password;

        if(!Hash::check($pwd, $getHashPwd))
        {
            return response()->json(['message' => '輸入的帳號或密碼錯誤']);
        }

        $new_token = str::random(10);
        $check_account -> update(['api_token' => $new_token]);
        $now_account = User::where('account', $request->account)->first();

        return response()->json
        ([
            'message' => '登入成功',
            'date' => $now_account,
        ]);
    }

    public function logout()
    {
//        $input=$request['api_token'];
//        $user = User::where('account',$request['account'])->first();
        $user = Auth::user();
//        dd($input);
        $removeToken = NULL;

        $user->update(['api_token' => $removeToken]);

        return response()->json(['message' => $user]);

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $allUser = User::paginate(3);
        // 每三名用戶一頁

        $allUser = User::all();

        return response()->json
        ([
            'message' => '所有的使用者資料',
            'date' => $allUser,
        ]);
    }

    public function suspend($id){
        $topAdmin = Auth::user();
        $role = $topAdmin -> role;

        if($role == '1'){
            $user = User::where('id',$id)->first();
                $user-> update(['status' => 'down']);
            return response()->json(['message' => '已將指定的使用者停權']);
        }
        return response()->json(['message' => '你沒有權限執行']);
    }


    public function restore($id){
        $topAdmin = Auth::user();
        $role = $topAdmin -> role;
        if($role == '1'){
            $user = User::where('id',$id)->first();
            $user-> update(['status' => 'up']);
            return response()->json(['message' => '已將指定的使用者恢復權限']);
        }
        return response()->json(['message' => '你沒有權限執行']);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();

        return response()->json(["message" => "當前使用者", "date" =>$user]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
