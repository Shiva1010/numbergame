<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Room extends Model
{
    use Notifiable;

    protected $fillable=[
        'player1',
        'player2',
        'number1',
        'number2',
        'winner',

    ];

}
