<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('user/register','UserController@register');     // 會員註冊
Route::post('user/login','UserController@login');           // 會員登入

Route::get('game/rank','GameController@index');                 // 遊戲排名

Route::middleware('auth:api')->prefix('game')->group(function(){
    Route::get('join','GameController@join');               // 加入遊戲
    Route::get('roomdate/{id}','GameController@roomDate');  // 遊戲房間狀況
    Route::get('havenumber/{id}','GameController@havenumber');  // 取得數值
    Route::get('user/logout','UserController@logout');      // 會員登出
    Route::get('user/show','UserController@show');          // 當前使用者資訊
    Route::get('over/{id}','GameController@over');                // 遊戲結果

    Route::post('play','GameController@play');
    Route::post('result','GameController@result');
});

Route::post('admin/register','AdminController@store');      // 管理員註冊
Route::post('admin/login','AdminController@login');         // 管理員登入

Route::middleware('auth:admin')->prefix('admin')->group(function(){
    Route::get('alluser','UserController@index');           // 所有會員資料
    Route::get('show','AdminController@show');              // 管理員資料
    Route::get('logout','AdminController@logout');          // 管理員登出
    Route::get('suspend/{id}','UserController@suspend');    // 會員停權
    Route::get('restore/{id}','UserController@restore');    // 會員恢復權限

    Route::post('store','UserController@store');
    Route::get('show','UserController@show');
    Route::put('update','UserController@update');
    Route::delete('delete','UserController@destroy');
});


