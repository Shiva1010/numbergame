<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  首頁，確認要註冊還是登入
Route::get('/index',function (){return view('index');});

//  會員註冊頁面
Route::get('/register',function (){return view('user/register');});

//  會員登入頁面
Route::get('/login',function (){return view('user/login');});

//  遊戲排名頁面
Route::get('/gamelobby', function (){return view('game/gamelobby');});

//  遊戲頁面
Route::get('/gametime',function (){return view('game/gametime');});


//  管理員註冊、登入、所有會員資料查看
Route::prefix('/admin')->group(function (){
    Route::get('/index',function (){return view('admin/index');});
    Route::get('/register',function (){return view('admin/register');});
    Route::get('/login',function (){return view('admin/login');});
    Route::get('/backstage',function (){return view('admin/backstage');});
    Route::get('/high_backstage', function (){return view('admin/high_backstage');});
});


// 測試用頁面
Route::get('/test',function (){
    return view('test');
});

