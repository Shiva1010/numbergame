$(function () {

    $('#adminLogout').on('click', function () {
        var key = JSON.parse(localStorage.getItem('token'));
        $.ajax({
            url: '/api/admin/logout',       // 要傳送的頁面
            method: 'GET',
            dataType: 'json',
            beforeSend: function (request) {
                console.log(request);
                request.setRequestHeader('Authorization', `Bearer ${key}`);
            },
            success: function (logout) {
                console.log(logout);
                alert("登出 Backstage");
                location.href='/admin/index';

            }, error: function (err) {
                console.log(err);
                alert("發生問題");

            }
        })
    });
})
