$(function () {
    // 顯示使用者資料
    var key = JSON.parse(localStorage.getItem('token'));

    $.ajax({
        url: '/api/game/user/show',
        method: 'GET',
        dataType: 'json',

        beforeSend: function (request) {
            console.log(request);
            request.setRequestHeader('Authorization', `Bearer ${key}`);
        },

        success: function (response) {
            console.log(response.date);
            $("#oldToken").val(response.date.api_token);

            $('#user_date').html(
                "<div style=\"text-align: -moz-left\"><h1>Welcome &nbsp"
                + response.date.name
                + "</h1></div><br>") ;

            $('#user_score').html(
                "<div style=\"text-align: -moz-left\"><h2>Now Score: "
                + response.date.score
                + "</h2></div>");

        }, error: function (err) {
            console.log(err);
            location.href = "/index";
        }
    })
})
