$(function () {
    // 會員登入
    $('#userLogin').on('click', function () {
        var loginAccount = $('#loginAccount').val();
        var loginPassword = $('#loginPassword').val();
        $.ajax({
            url: '/api/user/login',       // 要傳送的頁面
            method: 'POST',               // 使用 POST 方法傳送請求
            dataType: 'json',             // 回傳資料會是 json 格式
            data: {
                'account': loginAccount,
                'password': loginPassword,
            },

            success: function (response) {
                console.log(response)
                if(response.message == "登入成功" ) {
                    alert("成功登入")
                    // console.log(response.date.api_token);
                    $("#loginToken").val(response.date.api_token);
                    //  如果要 input 取得到值需要這樣帶
                    localStorage.setItem('token', JSON.stringify(response.date.api_token));
                    $('#userToken').val(response.date['api_token']);
                    location.href = '/gamelobby';
                }else{
                    alert('帳號或密碼錯誤，登入失敗');
                }

            }, error: function (err) {
                console.log(err)
                alert('輸入格式錯誤, 登入失敗');
            }
        });
    })

        // 已登入會員，直接轉往遊戲大廳
        var key = JSON.parse(localStorage.getItem('token'));

        $.ajax({
            url: '/api/game/user/show',  // roomId 房號索引用
            method: 'GET',
            dataType: 'json',

            beforeSend: function (request) {
                console.log(request);
                request.setRequestHeader('Authorization', `Bearer ${key}`);
            },

            success: function (response) {
                console.log(response.date);
                alert(response.date.name + " 目前已經是登入狀態，前往遊戲大廳")
                location.href = '/gamelobby';

            }, error: function (err) {
                console.log(err);
            }
        })

})




