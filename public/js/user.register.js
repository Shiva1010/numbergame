$(function(){
    // 遊戲註冊
    $('#userRegister').on('click', function () {
         var name = $('#name').val();    // 將常數轉換變數
         var account = $('#account').val();
         var password = $('#password').val();
         $.ajax({
             url: '/api/user/register',       // 要傳送的頁面
             method: 'POST',               // 使用 POST 方法傳送請求
             dataType: 'json',             // 回傳資料會是 json 格式
             data: {
                 'account': account,
                 'password': password,
                 'name': name,
             },      // 設定要傳送的 date
             success: function (response) {
                 if(response) {
                     alert("會員註冊成功，返回首頁")
                     console.log(response);
                     location.href='/index';
                 }
             }, error: function (err) {
                 alert('輸入格式錯誤或是已經有人註冊了');
                 console.log(err);
             }
         });
     });

 });
