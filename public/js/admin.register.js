 $(function() {
     $('#adminRegister').on('click', function () {
         var name = $('#name').val();    // 將常數轉換變數
         var account = $('#account').val();
         var password = $('#password').val();
         $.ajax({
             url: '/api/admin/register',       // 要傳送的頁面
             method: 'POST',               // 使用 POST 方法傳送請求
             dataType: 'json',             // 回傳資料會是 json 格式
             data: {
                 'account': account,
                 'password': password,
                 'name': name,
             },      // 設定要傳送的 date
             success: function (response) {
                 alert("管理員註冊成功，即將返回首頁")
                 location.href = '/admin/index';
             }, error: function () {
                 alert('輸入格式錯誤或是已經有人註冊了');
             }
         });
     });

 });
