$(function() {
        // 管理員登入
        $('#adminLogin').on('click', function() {
            var loginAccount = $('#loginAccount').val();
            var loginPassword = $('#loginPassword').val();
            $.ajax({
                url: '/api/admin/login',       // 要傳送的頁面
                method: 'POST',               // 使用 POST 方法傳送請求
                dataType: 'json',             // 回傳資料會是 json 格式
                data: {'account': loginAccount, 'password': loginPassword},      // 設定要傳送的 date
                success: function (response) {
                    if (response.message == "登入成功") {
                        if (response.date['role']== '1'){
                            alert('Master 登入成功');
                            localStorage.setItem('token', JSON.stringify(response.date.api_token));
                            location.href = '/admin/high_backstage';
                            return false;
                        }
                        alert("成功登入");
                        $('#name').html(response.date.name);
                        $('#admin_name').html(
                            "<div style=\"text-align: center\"><h1>Wellcome &nbsp"
                            + response.date.name
                            + "</h1></div>");
                        localStorage.setItem('token', JSON.stringify(response.date.api_token));
                        location.href = '/admin/backstage';
                    } else {
                        alert('帳號或密碼錯誤，登入失敗');
                    }
                }, error: function (err) {
                    console.log(err);
                    alert('輸入格式錯誤, 登入失敗');
                }
            });
        });

        // 已登入則自動切換到後臺
        var key = JSON.parse(localStorage.getItem('token'));

        $.ajax({
            url: '/api/admin/show',
            method: 'GET',
            dataType: 'json',

            beforeSend: function (request) {
                console.log(request);
                request.setRequestHeader('Authorization', `Bearer ${key}`);
            },

            success: function (response) {
                if (response.date.role== '1'){
                    alert('Master 已登入');
                    location.href = '/admin/high_backstage';
                    return false;
                }
                alert('Sir '+ response.date.name + ' 目前已是登入狀態，前往管理頁面');
                location.href = '/admin/backstage'
                return false;
            }, error: function (err) {
                console.log(err);
            }
        })
})
