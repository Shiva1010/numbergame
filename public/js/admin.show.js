$(function() {
    // 目前的 Admin 資訊
    var key = JSON.parse(localStorage.getItem('token'));

    $.ajax({
        url: '/api/admin/show',
        method: 'GET',
        dataType: 'json',

        beforeSend: function (request) {
            console.log(request);
            request.setRequestHeader('Authorization', `Bearer ${key}`);
        },

        success: function (response) {

            console.log(response.date);
            $('#admin_date').html(
                "<div style=\"text-align: -moz-left\"><h1>Welcome Sir &nbsp"
                + response.date.name
                + "</h1></div>");

        }, error: function (err) {
            console.log(err);
            location.href = "/admin/index";
        }
    })

});
