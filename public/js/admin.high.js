$(function() {
        // 非最高權限者直接跳轉回一般管理者頁面
        var key = JSON.parse(localStorage.getItem('token'));

        $.ajax({
            url: '/api/admin/show',
            method: 'GET',
            dataType: 'json',

            beforeSend: function (request) {
                console.log(request);
                request.setRequestHeader('Authorization', `Bearer ${key}`);
            },

            success: function (response) {
                if (response.date.role== '0') {
                    location.href = '/admin/backstage'
                    return false;
                }
            }, error: function (err) {
                console.log(err);
            }
        })
})
