$(function () {
    '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">'
    '<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>'
    '<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>'
    '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>'

    $('#adminRank').click(function () {
        $.ajax({
            url: '/api/game/rank',
            method: 'GET',
            cache: false,
            async: false,
            dataType: 'json',
            success: function (rank) {
                // alert('查看遊戲排行榜');
                var list = rank.date, t = null;

                $('#backstage').html(
                    '<div class="col-sm-12" style="text-align: center">'
                    + '<table class="table table table-dark">'
                    + '<thead>'
                    + '<tr class="table-active">'
                    + '<th width="120px" align="center">Rank</th>'
                    + '<th width="150px" align="center">Acoount</th>'
                    + '<th width="150px" align="center">Score</th>'
                    + '</tr>'
                    + '</thead>'
                    + '</table>'
                    + '</div>'
                );
                for (var i = 0; i < list.length; i++) {
                    console.log(list);
                    if (t == null) {
                        t = list[i];
                        t.idx = 1;

                    } else {
                        var n = list[i];
                        if (n["score"] == t["score"]) {
                            n.idx = t.idx;
                        } else {
                            n.idx = i + 1;
                        }
                        t = n;
                    }

                    $('#backstage').append(
                        '<div class="col-sm-12" style="text-align: center" >'
                        + '<table class="table table-sm table-dark">'
                        + '<tbody>'
                        + '<tr>'
                        + '<th width="120px" align="center">' + t.idx + '</th>'
                        + '<th width="150px" align="center">' + t["account"] + '</th>'
                        + '<th width="150px" align="center">' + t["score"] + '</th>'
                        + '</tr>'
                        + '</tbody>'
                        + '</table>'
                        + '</div>'
                    );
                }

            },error: function (err) {
                console.log(err);
            }
        })
    })
});
