$(function () {

    '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">'
    '<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>'
    '<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>'
    '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>'

    '<link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&display=swap" rel="stylesheet"> '
    '<link href="https://fonts.googleapis.com/css2?family=Cabin+Sketch:wght@700&family=Gloria+Hallelujah&display=swap" rel="stylesheet">'


    $('#lookUser').click (function(){
        var key = JSON.parse(localStorage.getItem('token'));
        $.ajax({
            url: '/api/admin/alluser',       // 要傳送的頁面
            method: 'GET',
            dataType: 'json',
            beforeSend: function(request) {
                console.log(request);
                request.setRequestHeader('Authorization', `Bearer ${key}`);
            },
            success: function(allDate){
                console.log(allDate);
                // alert("查看所有會員資料");
                    $('#backstage').html(
                        '<div class="col-sm-12" style="text-align: center">'
                        + '<table class="table table-striped table-dark" >'
                        + '<thead>'
                        + '<tr class="table-active">'
                        + '<th width="10%"  align="center">id</jth>'
                        + '<th width="30%" align="center">account</th>'
                        + '<th width="20%" align="center">name</th>'
                        + '<th width="20%" align="center">score</th>'
                        + '<th width="10%"  align="center">win</th>'
                        + '<th width="10%"  align="center">status</th>'
                        + '</tr>'
                        + '</thead>'
                        + '</table>'
                        + '</div>'
                    );

                    for (var i = 0; i < allDate.date.length; i++) {
                        console.log(allDate);
                        $('#backstage').append(
                            '<div class="col-sm-12" style="text-align: center" >'
                            + '<table class="table table-striped table-dark" >'
                            + '<tbody>'
                            + '<tr>'
                            + '<th width="10%" align="center">' + allDate.date[i]["id"] + '</th>'
                            + '<th width="30%" align="center">' + allDate.date[i]["account"] + '</th>'
                            + '<th width="20%" align="center">' + allDate.date[i]["name"] + '</th>'
                            + '<th width="20%" align="right">' + allDate.date[i]["score"] + '</th>'
                            + '<th width="10%" align="right">' + allDate.date[i]["win"] + '</th>'
                            + '<th width="10%" align="center">' + allDate.date[i]["status"] + '</th>'
                            + '</tr>'
                            + '</tbody>'
                            + '</table>'
                            + '</div>'
                        );

                    }
            },error: function (err) {
                console.log(api_token);
                alert('無權限查看');
            }
        });
        return false;  // 阻止瀏覽器跳轉到 send.php，因為已經用 ajax 送出去了
    });


})




