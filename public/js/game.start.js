$(function () {
    '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">'
    '<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>'
    '<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>'
    '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>'

    // 遊戲排名

        $.ajax({
            url: '/api/game/rank',
            method: 'GET',
            cache: false,
            async: false,
            dataType: 'json',
            success: function (rank) {

                var list = rank.date, t = null;

                $('#leaderboard').append(
                    '<div class="col-sm-12" style="text-align: center">'
                    + '<table class="table table">'
                    + '<thead>'
                    + '<tr class="table-active">'
                    + '<th width="120px" align="center">Rank</th>'
                    + '<th width="150px" align="center">Acoount</th>'
                    + '<th width="150px" align="center">Score</th>'
                    + '</tr>'
                    + '</thead>'
                    + '</table>'
                    + '</div>'
                );
                for (var i = 0; i < list.length; i++) {
                    console.log(list);
                    if (t == null) {
                        t = list[i];
                        t.idx = 1;

                    } else {
                        var n = list[i];
                        if (n["score"] == t["score"]) {
                            n.idx = t.idx;
                        } else {
                            n.idx = i + 1;
                        }
                        t = n;
                    }

                    $('#leaderboard').append(
                        '<div class="col-sm-12" style="text-align: center">'
                        + '<table class="table table-sm">'
                        + '<tbody>'
                        + '<tr>'
                        + '<th width="120px" align="center">' + t.idx + '</th>'
                        + '<th width="150px" align="center">' + t["account"] + '</th>'
                        + '<th width="150px" align="center">' + t["score"] + '</th>'
                        + '</tr>'
                        + '</tbody>'
                        + '</table>'
                        + '</div>'
                    );
                }

            }
        })


    // 加入遊戲，並轉向遊戲房間
    $('#joinRoom').on('click', function () {
        var key = JSON.parse(localStorage.getItem('token'));
        $.ajax({
            url: '/api/game/join',       // 要傳送的頁面
            method: 'GET',
            dataType: 'json',
            beforeSend: function (request) {
                console.log(request);
                request.setRequestHeader('Authorization', `Bearer ${key}`);
            },

            success: function (roomDate) {

                if (roomDate.message == '停權中'){
                    alert('停權中，無遊戲權限，請洽管理者');
                    location.href = '/gamelobby';
                    return false;
                }
                console.log(roomDate);
                alert(roomDate.message);
                localStorage.setItem('roomId', JSON.stringify(roomDate.date.id));
                location.href = '/gametime';

            }, error: function (err) {
                console.log(err);
                alert('無遊戲權限');
            }
        });
        return false;  // 阻止瀏覽器跳轉到 send.php，因為已經用 ajax 送出去了
    });

    // 登出
    $('#logout').on('click', function () {
        var key = JSON.parse(localStorage.getItem('token'));
        $.ajax({
            url: '/api/game/user/logout',       // 要傳送的頁面
            method: 'GET',
            dataType: 'json',
            beforeSend: function (request) {
                console.log(request);
                request.setRequestHeader('Authorization', `Bearer ${key}`);
            },

            success: function (logout) {
                console.log(logout);
                alert("登出遊戲");
                location.href='index';

            }, error: function (err) {
                console.log(err);
                alert("發生問題");
            }
        })
    });


})



