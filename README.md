# NumberGame
###### tags: `後端作品集`



---
## 功能一覽表

Domain Name：
位址：

1. 會員註冊
2. 會員登入
3. 會員登出
4. 加入遊戲
5. 查看遊戲房間狀況
6. 取得號碼
7. 結束遊戲 ( 計算得分 )  
8. 管理員註冊
9. 管理員登入
10. 查看所有會員資料 







### 一、會員註冊
  
  * 方式：POST
  * 驗證方式：無
  * api路徑：/api/user/register
    

| key      | 測試資料       | 必須符合的規則|
| -------- | -------       |------- 
| name     | Hi            |string
| account  | hiworld       |string
| password | qwer1234      |string, 6~12位數字






:::success
正確版
* Respose:
```jsonld=
{
    "message": "會員註冊成功",
    "date": {
        "name": "Hi",
        "account": "hiworld",
        "score": 0,
        "competition": 0,
        "win": 0,
        "updated_at": "2020-06-16T02:13:36.000000Z",
        "created_at": "2020-06-16T02:13:36.000000Z",
        "id": 14
    }
}
```

:::


---

### 二、會員登入

  * 方式：POST
  * 驗證方式：無
  * api路徑：/api/uesr/login
    

| key      | 測試資料   | 必須符合的規則|
| -------- | -------   |------- 
| account  | hiworld   |string
| password | qwer1234  |string, 6~12位數字





:::success
正確版
* Respose:
```json
{
    "message": "登入成功",
    "date": {
        "id": 14,
        "name": "Hi",
        "account": "hiworld",
        "score": 0,
        "competition": 0,
        "win": 0,
        "api_token": "OaS3OtUxGd",
        "created_at": "2020-06-16T02:13:36.000000Z",
        "updated_at": "2020-06-16T02:16:37.000000Z"
    }
}
```

---
### 三、會員登出

  * 方式：GET
  * 驗證方式：api-token
  * api路徑：/api/game/user/logout
   
   
:::success
正確版
* Respose:
```json
{
    "message": {
        "id": 1,
        "name": "Kuru",
        "account": "Kururu",
        "score": 10,
        "competition": 2,
        "win": 1,
        "api_token": null,
        "created_at": "2020-06-05T03:19:31.000000Z",
        "updated_at": "2020-06-16T03:21:01.000000Z"
    }
}
```

---
### 四、加入遊戲

  * 方式：GET
  * 驗證方式：api-token
  * api路徑：/api/game/join
    

:::success
正確版
* Respose:

1. 如果沒有 (玩家2) 的空缺房，則新建一個房間，並且身份是 （玩家1)
```json
{
    "message": "新遊戲房建立",
    "date": {
        "player1": "hiworld",
        "updated_at": "2020-06-16T02:34:13.000000Z",
        "created_at": "2020-06-16T02:34:13.000000Z",
        "id": 41
    }
}
```

2. 如果有 (玩家2) 的空缺房，則直接入房，身份是 （玩家2)
```json
{
    "message": "加入遊戲成功",
    "date": {
        "id": 41,
        "player1": "hiworld",
        "player2": "Kururu",
        "number1": null,
        "number2": null,
        "winner": null,
        "created_at": "2020-06-16T02:34:13.000000Z",
        "updated_at": "2020-06-16T02:42:49.000000Z"
    }
}
```


---
### 五、查看遊戲房間狀況

  * 方式：GET
  * 驗證方式：api-token
  * api路徑：/api/game/roomdate/{id}
    

:::success
正確版
* Respose:

```json
{
    "message": "遊戲房間狀況",
    "date": [
        {
            "id": 41,
            "player1": "hiworld",
            "player2": "Kururu",
            "number1": null,
            "number2": null,
            "winner": null,
            "created_at": "2020-06-16T02:34:13.000000Z",
            "updated_at": "2020-06-16T02:42:49.000000Z"
        }
    ]
}
```


---
### 六、取得遊戲號碼

  * 方式：GET
  * 驗證方式：api-token
  * api路徑：/api/game/havenumber/{id}
    

:::success
正確版
* Respose:

1. (玩家1) 取得號碼 
```json
{
    "message": "P1玩家取得數字",
    "date": [
        "4",
        null
    ]
}
```

2. (玩家2) 取得號碼 
```json
{
    "message": "P2玩家取得數字",
    "date": [
        null,
        "8"
    ]
}
```

---
### 七、遊戲結束（結算）

  * 方式：GET
  * 驗證方式：api-token
  * api路徑：/api/game/end/{id}
    

:::success
正確版
* Respose:

1. Winer
```json
{
    "message": "you win",
    "number1": "4",
    "number2": "8"
}
```

2. loser
```json
{
    "message": "you lose",
    "number1": "4",
    "number2": "8"
}
```

3. 平手
```json
{
    "message": "此局平手",
    "number1": "7",
    "number2": "7"
}
```

---

### 八、管理員註冊
  
  * 方式：POST
  * 驗證方式：無
  * api路徑：/api/admin/register
    

| key      | 測試資料       | 必須符合的規則|
| -------- | -------       |------- 
| name     | king          |string
| account  | kingkong      |string
| password | qaz123        |string, 6~12位數字



:::success
正確版
* Respose:
```jsonld=
{
    "message": "管理員帳戶註冊成功",
    "date": {
        "name": "king",
        "account": "kingkong",
        "updated_at": "2020-06-16T03:24:12.000000Z",
        "created_at": "2020-06-16T03:24:12.000000Z",
        "id": 9
    }
}
```

:::


---

### 九、管理員登入

  * 方式：POST
  * 驗證方式：無
  * api路徑：/api/admin/login
    

| key      | 測試資料   | 必須符合的規則|
| -------- | -------   |------- 
| account  | kingkong  |string
| password | qaz123    |string, 6~12位數字





:::success
正確版
* Respose:
```json
{
    "message": "登入成功",
    "date": {
        "id": 9,
        "account": "kingkong",
        "name": "king",
        "api_token": "SMAEmjdTNv",
        "created_at": "2020-06-16T03:24:12.000000Z",
        "updated_at": "2020-06-16T03:25:29.000000Z"
    }
}
```

---

### 十、查看所有會員資料

  * 方式：GET
  * 驗證方式：admin-token
  * api路徑：/api/admin/alluser
    

:::success
正確版
* Respose:
```json
{
    "message": "現有使用者資料",
    "date": [
        {
            "id": 1,
            "name": "Kuru",
            "account": "Kururu",
            "score": 10,
            "competition": 2,
            "win": 1,
            "api_token": null,
            "created_at": "2020-06-05T03:19:31.000000Z",
            "updated_at": "2020-06-16T03:21:01.000000Z"
        },
        {
            "id": 2,
            "name": "Tama",
            "account": "Tamama",
            "score": 0,
            "competition": 1,
            "win": 0,
            "api_token": null,
            "created_at": "2020-06-05T03:19:49.000000Z",
            "updated_at": "2020-06-12T07:39:41.000000Z"
        },
        "// 以下多筆資料，依此類推"
      ]
}

```
